@extends('layouts.master')

@section('title')
    Registro
@endsection

@section('header')
    @include('partials.header')
@endsection

@section('content')
    <main class="Ingresar  u-afterFixed">
        <form id="Ingresar-form" method="POST" action="{{ route('register') }}" class="Form">
            <h2 class="Form-title">Regístrate</h2>
            {{ csrf_field() }}
            <div class="Form-element">
                <label for="name"><i class="fa fa-user"></i></label>
                <input type="text" name="name" id="name" value="{{ old('name') }}" required autofocus placeholder="Nombre y Apellidos">
                @if ($errors->has('name'))
                    <div class="Form-message  u-error">
                        <strong>{{ $errors->first('name') }}</strong>
                    </div>
                @endif
            </div>
            <div class="Form-element">
                <label for="email"><i class="fa fa-envelope-o"></i></label>
                <input type="email" name="email" id="email" value="{{ old('email') }}" required placeholder="Correo electrónico">
                @if ($errors->has('email'))
                    <div class="Form-message  u-error">
                        <strong>{{ $errors->first('email') }}</strong>
                    </div>
                @endif
            </div>
            <div class="u-flex-space-between">
                <div class="Form-element">
                    <label for="password"><i class="fa fa-lock"></i></label>
                    <input type="password" name="password" id="password" required placeholder="Contraseña">
                    @if ($errors->has('password'))
                        <div class="Form-message  u-error">
                            <strong>{{ $errors->first('password') }}</strong>
                        </div>
                    @endif
                </div>
                <div class="Form-element">
                    <label for="password_confirmation"><i class="fa fa-lock"></i></label>
                    <input type="password" name="password_confirmation" id="password-confirm" placeholder="Confirmar contraseña" required>
                </div>
            </div>
            <div class="Form-element  u-bg-success">
                <input type="submit" value="Enviar">
            </div>
        </form>
        <aside class="Ingresar-registro">
            <p>
                ¿Ya tienes una cuenta?, <a href="{{ route('login') }}">Inicia sesión</a>
            </p>
        </aside>
    </main>
@endsection
