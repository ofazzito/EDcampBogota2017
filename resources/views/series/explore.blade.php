@extends('layouts.master')

@section('title')
    Explorar
@endsection

@section('header')
    @include('partials.header')
    @include('partials.user_menu')
@endsection

@section('content')
    <main class="Explorar  u-afterFixed">
        <aside id="modal-tags" class="Explorar-aside">
            <form id="searchForm" action="{{ route('explore') }}" method="get" class="Search" >
                <div class="Form-element">
                    <label for="search"><i class="fa fa-search"></i></label>
                    <input type="search" name="search" id="search" placeholder="Buscar" value="{{ Request::get('search') }}">
                </div>
            </form>
            <ul class="TagsList">
                @foreach($tags as $tag)
                    <li class="TagsList-item">
                        <a href="{{ route('explore') }}?tag={{ $tag->name }}" class="TagsList-link">
                            <i class="fa fa-tag"></i>
                            {{ $tag->name }}
                        </a>
                    </li>
                @endforeach
            </ul>
        </aside>
        <article class="Explorar-main">
            <header class="Explorar  u-title">
                <h2>Explorar</h2>
                <a href="#modal-tags" data-modal-open><i class="fa fa-ellipsis-v"></i></a>
            </header>
            <section class="Cards">
                @foreach($series as $serie)
                    <figure class="Card">
                        <div class="Card-image">
                            @if($serie->picture)
                                <img src="{{ Storage::url($serie->picture) }}" alt="{{ $serie->name }}">
                            @endif
                        </div>
                        <figcaption class="Card-title">
                            <h4>{{ $serie->name }}</h4>
                            <div class="u-smaller">
                                <a href="{{ route('series.show', $serie) }}" class="Card-link">
                                    Detalles
                                </a>
                            </div>
                        </figcaption>
                    </figure>
                @endforeach
            </section>
        </article>
    </main>

    <div class="modal">
        <div class="modal-inner">
            <span data-modal-close>&times;</span>
            <div class="modal-content"></div>
        </div>
    </div>
@endsection
