//require('./components/bootstrap')
import { navigation } from './components/header.js'
import { explore } from './components/explore.js'


if ( document.querySelector('.Header-container').querySelector('.Panel') ) {
  navigation()
}

if ( location.pathname.includes('explore') ) {
  explore()
}

if ( document.getElementById('picture') ) {
  let picture = document.getElementById('picture')
  picture.addEventListener('change', e => {
    picture.parentElement.classList.add('u-bg-success')
  })

}
